sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "sap/ui/model/json/JSONModel",
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/m/Token'
], function (Controller, JSONModel, MessageToast, Fragment, Token) {
    "use strict";

    return Controller.extend("SapUI5Tutorial.Application.Main.views.addTicket", {
        onInit: function () {
            var that = this

            var oData = {
				"SelectedOncelik": "3oncelik",
				"OncelikCollection": [
					{
						"idO": "5oncelik",
						"name": "Çok Yüksek"
					},
					{
						"idO": "4oncelik",
						"name": "Yüksek"
					},
					{
						"idO": "3oncelik",
						"name": "Orta"
					},
					{
						"idO": "2oncelik",
						"name": "Düşük"
					},
					{
						"idO": "1oncelik",
						"name": "Çok Düşük"
					},					
                ],
                "SelectedDurum": "YeniDurum",
                "DurumCollection": [
					{
						"idD": "YeniDurum",
						"name": "Yeni"
					},
					{
						"idD": "AnalizDurum",
						"name": "Analiz"
					},
					{
						"idD": "IsleniyorDurum",
						"name": "Işleniyor"
					},
					{
						"idD": "TestDurum",
						"name": "Test"
					},
					{
						"idD": "ProblemDurum",
						"name": "Problem"
                    },
                    {
						"idD": "AraDurum",
						"name": "Ara Verildi"
                    },
                    {
						"idD": "TamamDurum",
						"name": "Tamamlandı"
                    },
                    {
						"idD": "KapatDurum",
						"name": "Kapatıldı"
                    },
                    {
						"idD": "IptalDurum",
						"name": "Iptal Edildi"
					},		
				],
				"Editable": true,
				"Enabled": true
            };
            
            var oModel = new JSONModel(oData);            
            this.getView().setModel(oModel);
            
            databaseHandler.createToDoDB();
            databaseHandler.createToDoSDB();
            databaseHandler.createUserDB();
            this.getAllUsers();
            this.getAllProject();

            AddTicket.MultiInput(this, "vbox", "multiinput");
            AddTicket.MultiInput(this, "vbox2", "multiinput2");

            var oMultiInput2 = sap.ui.getCore().byId("multiinput2");
            oMultiInput2.setTokens([
                new Token({ text: localStorage.username, key: "0001" }),
            ]);

            sap.ui.require(["sap/ui/richtexteditor/RichTextEditor", "sap/ui/richtexteditor/EditorType"],
                function (RTE, EditorType) {                    
                    var oRichTextEditor = new RTE("myRTE", {
                        editorType: EditorType.TinyMCE4,
                        width: "100%",
                        height: "600px",
                        customToolbar: true,
                        showGroupFont: true,
                        showGroupLink: true,
                        showGroupInsert: true,
                        value: "",
                        ready: function () {
                            this.addButtonGroup("styleselect").addButtonGroup("table");
                        }
                    });

                    that.getView().byId("idVerticalLayout").addContent(oRichTextEditor);
                });
        },
        
        getAllUsers: function () {
            AddTicket.selectKimden().then(function (fullfilled) {
                var rows = Object.assign([], fullfilled.rows)
                oModel.setProperty("/userList", rows);
            })
        },

        onCancel: function(){
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("ToDo/Todo");
            location.reload();
        },

        onAccept: function(){
            sap.ui.getCore().byId("multiinput").getTokens();
            sap.ui.getCore().byId("multiinput2").getTokens();
            this.getView().byId("konu").getValue();
        },

        getAllProject: function(){
            Project.TableProject().then(function(fullfilled){
                var rows = Object.assign([],fullfilled.rows);
                oModel2.setProperty("/projectList",rows);                
            })
        }
    });
});