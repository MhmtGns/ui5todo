sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/core/format/DateFormat",
	"sap/ui/unified/Menu",
	"sap/ui/unified/MenuItem",
	"sap/m/Menu",
	"sap/m/MenuItem",
	"sap/ui/core/Fragment"
], function (Controller, JSONModel, MessageToast, DateFormat, Menu, MenuItem, MenuM, MenuItemM, Fragment) {
	"use strict";
	var rowid;
	return Controller.extend("SapUI5Tutorial.Application.Main.views.project", {
		
		onInit: function () {
			databaseHandler.createProjectDB();
			databaseHandler.createToDoDB();
			this.getAllUsers();
			this.getAllProjects();


			this.byId("table").setContextMenu(new MenuM({
				items: [
					new MenuItemM({
						icon: "sap-icon://edit",
						text: "Düzenle",
						press: function (oEvent) {
							var item = oEvent.getSource().getParent()
							var sPath = item.getBindingContext().sPath
							var tableElement = oModel.getProperty(sPath)
							if (!window._oAddProDialog) {
								window._oAddProDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.Main.fragments.addProject", this);
							}
							window._oAddProDialog.open();
							sap.ui.getCore().byId("onSaveAddPro").setVisible(false);
							sap.ui.getCore().byId("onSaveEditPro").setVisible(true);
							sap.ui.getCore().byId("projectName").setValue(tableElement.projectName)
							sap.ui.getCore().byId("projectManager").setValue(tableElement.projectSupervisor)
							sap.ui.getCore().byId("beganDate").setValue(tableElement.beganDate)
							sap.ui.getCore().byId("finishDate").setValue(tableElement.finishDate)
							rowid = tableElement.rowid;
						}
					}),
					new MenuItemM({
						text: "Sil",
						icon: "sap-icon://delete",
						press: function (oEvent) {
							var item = oEvent.getSource().getParent()
							var sPath = item.getBindingContext().sPath
							var tableElement = oModel.getProperty(sPath)
							Project.RemoveProject(tableElement.rowid).then(function (fullfilled) {
								MessageToast.show(fullfilled);
							})
							this.getAllProjects();
						}
					})
				]
			}));
		},
		cellContext: function (oEvent) {
			this.byId("table").setContextMenu(new MenuM({
				items: [
					new MenuItemM({ text: "{rowid}" }),
					new MenuItemM({ text: "{projectName}" })
				]
			}));

		},
		handleToggleSecondaryContent: function (oEvent) {
			var oSplitContainer = this.byId("mySplitContainer");
			oSplitContainer.setShowSecondaryContent(!oSplitContainer.getShowSecondaryContent());
		},

		onAddProject: function (oEvent) {
			if (!window._oAddProDialog) {
				window._oAddProDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.Main.fragments.addProject", this);
			}
			window._oAddProDialog.open();

			sap.ui.getCore().byId("onSaveEditPro").setVisible(false);
			sap.ui.getCore().byId("onSaveAddPro").setVisible(true);
			sap.ui.getCore().byId("projectManager").setValue(localStorage.username);
		},

		onCancelPro: function (oEvent) {
			window._oAddProDialog.close(oEvent.getSource());
		},

		onSavePro: function (oEvent) {

			var projectName = sap.ui.getCore().byId("projectName").getValue();
			var projectManager = sap.ui.getCore().byId("projectManager").getValue();
			var beganDate = sap.ui.getCore().byId("beganDate").getValue()
			var finishDate = sap.ui.getCore().byId("finishDate").getValue()
			if (projectName != "" && beganDate != "" && finishDate != "" && projectManager != "") {
				Project.AddProject(projectName, projectManager, beganDate, finishDate).then(function (fullfilled) {
					MessageToast.show(fullfilled);
				})
			}
			else {
				MessageToast.show("Lütfen tüm alanları doldurunuz!");
			}
			var that = this;
			that.getAllProjects();

		},

		getAllUsers: function () {
			AddTicket.selectKimden().then(function (fullfilled) {
				var rows = Object.assign([], fullfilled.rows)
				oModel.setProperty("/userList", rows);
			})
		},

		getAllProjects: function () {
			Project.TableProject().then(function (fullfilled) {
				var rows = Object.assign([], fullfilled.rows);
				oModel.setProperty("/projectTable", rows);
			}).catch(function (error) {
				MessageToast.show(error);
			})
		},

		onOpenDialog: function () {
			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dialogAddProject")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "SapUI5Tutorial.Application.Main.fragments.addProject"
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dialogAddProject").open();
			}
		},

		onEditPro: function () {
			var name = sap.ui.getCore().byId("projectName").getValue()			
			var began = sap.ui.getCore().byId("beganDate").getValue()
			var finish = sap.ui.getCore().byId("finishDate").getValue()
			Project.EditProject(rowid,name,began,finish).then(function(fullfilled){
				MessageToast.show(fullfilled);
				
			}).catch(function(error){
				MessageToast.show(error);
			})
			var that = this;
			that.getAllProjects();
		}

	});

});