sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "sap/ui/model/json/JSONModel",
    'sap/m/MessageToast',
    'sap/ui/core/Fragment'
], function (Controller, JSONModel, MessageToast, Fragment) {
    "use strict";
    var haur = 1
    var now = new Date().getTime();
    return Controller.extend("SapUI5Tutorial.Application.Main.views.loginPage", {
        onInit: function () {
            databaseHandler.createUserDB();
            var setupTime = localStorage.getItem('setupTime');
            if (now - setupTime > haur * 60 * 60 * 1000) {
                localStorage.clear()
            }
        },

        onPressLogIn: function () {
            var _this = this;
            var username = this.getView().byId("username").getValue();
            var password = this.getView().byId("password").getValue();
            if (username == "") {
                this.getView().byId("username").setValueState(sap.ui.core.ValueState.Error);
            }
            else if (password == "") {
                this.getView().byId("password").setValueState(sap.ui.core.ValueState.Error);
            }
            else {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(username)) {
                    User.EmailLogin(username, password).then(function (fullfilled) {
                        MessageToast.show("wellcome email = " + username);
                        localStorage.setItem("username", username)
                        localStorage.setItem("setupTime", now)
                        var oRouter = sap.ui.core.UIComponent.getRouterFor(_this);
                        oRouter.navTo("ToDo/Employee");
                        location.reload();
                    }).catch(function (error) {
                        MessageToast.show(error)
                    })
                }
                else {
                    User.UsernameLogin(username, password).then(function (fullfilled) {
                        MessageToast.show("wellcome user = " + username)
                        localStorage.setItem("username", username)
                        localStorage.setItem("setupTime", now)
                        var oRouter = sap.ui.core.UIComponent.getRouterFor(_this);
                        oRouter.navTo("ToDo/Employee");
                        location.reload();
                    }).catch(function (error) {
                        MessageToast.show(error)
                    })
                }
            }
        }
    });

});