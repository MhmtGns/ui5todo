sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "sap/ui/model/json/JSONModel",
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/m/Dialog',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text'
], function (Controller, JSONModel, MessageToast, Fragment, Dialog, Button, ButtonType, Text) {
    "use strict";
    var rowid;
    return Controller.extend("SapUI5Tutorial.Application.Main.views.employee", {
        onInit: function () {
            databaseHandler.createUserDB();
            this.getAllEmployee();
        },

        onAddEmployee: function(oEvent){
            if (!this._oAddEmpDialog) {
                this._oAddEmpDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.Main.fragments.addEmployee",this);
            }
            this._oAddEmpDialog.open(oEvent.getSource());
        },

        handleExcelUpload: function (e) {
            var oFile = e.getParameter("files")[0];
            if (oFile) {
                var reader = new FileReader();
                reader.onload = function (evt) {
                    var data = evt.target.result;
                    oModel.setProperty('/imageData', data);
                };
                reader.readAsDataURL(oFile);
            }
        },

        onSaveAdd: function(oEvent){
            var employeeName  = sap.ui.getCore().byId("employeeNameAdd").getValue();            
            var employeeEmail = sap.ui.getCore().byId("employeeEmailAdd").getValue();
            var employeeUsername = employeeEmail.substring(0,employeeEmail.indexOf("@"));
            var employeePassword = "123456";
            var employeePosition = "Consultant";
            var employeeImage = oModel.getProperty('/imageData');

            User.AddEmployee(employeeName,employeeUsername,employeeEmail,employeePassword,employeePosition,employeeImage).then(function(fullfilled){
                MessageToast.show(fullfilled);                
            }).catch(function(error){
                MessageToast.show(error);
            })
        },

        onCancelAdd: function(oEvent){
            this._oAddEmpDialog.close(oEvent.getSource());
        },

        getAllEmployee: function(){
            User.ListEmployee().then(function(fullfilled){
                var rows = Object.assign([],fullfilled.rows)
                oModel.setProperty("/employeeList",rows);
            })
        },

        onEdit: function(oEvent){
            if(!this._oEditEmpDialog){
                this._oEditEmpDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.Main.fragments.editEmployee",this);
            }
            this._oEditEmpDialog.open(oEvent.getSource());

            var spath = oEvent.oSource.getBindingContext().sPath;

            var employeeName = oModel.getProperty(spath).name;
            var employeeEmail = oModel.getProperty(spath).email;
            var employeeImage = oModel.getProperty(spath).image;
            rowid = oModel.getProperty(spath).rowid;
            oModel.setProperty('/imageData', employeeImage);
            sap.ui.getCore().byId("employeeNameEdit").setValue(employeeName);
            sap.ui.getCore().byId("employeeEmailEdit").setValue(employeeEmail);

        },
        
        onSelectChanged:function(oEvent){
            var go = oEvent.mParameters.key;
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo(go);
        },

        onCancelEdit: function(oEvent){
            this._oEditEmpDialog.close(oEvent.getSource());
        },

        onSaveEdit: function(oEvent){
            var _this = this;
            var employeeName  = sap.ui.getCore().byId("employeeNameEdit").getValue();            
            var employeeEmail = sap.ui.getCore().byId("employeeEmailEdit").getValue();
            var employeeUsername = employeeEmail.substring(0,employeeEmail.indexOf("@"));            
            var employeeImage = oModel.getProperty('/imageData');
            User.EditEmployee(rowid,employeeName,employeeUsername,employeeEmail,employeeImage).then(function(fullfilled){
                MessageToast.show(fullfilled);
            })
            _this.getAllEmployee();
            _this._oEditEmpDialog.close(oEvent.getSource());
        },

        onDelete: function(oEvent){
            var _this = this;
            var spath = oEvent.oSource.getBindingContext().sPath;
            var dialog = new Dialog({
				title: 'Confirm',
				type: 'Message',
				content: new Text({ text: '' + oModel.getProperty(spath).username + " will be deleted? " }),
				beginButton: new Button({
					type: ButtonType.Emphasized,
					text: 'Submit',
					press: function () {
						User.DeleteEmployee(oModel.getProperty(spath).rowid).then(function(fullfilled){
                            MessageToast.show(fullfilled);
                        })
                        dialog.close();                        
                        _this.getAllEmployee();
					}
				}),
				endButton: new Button({
					text: 'Cancel',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
        }
        

    });
});