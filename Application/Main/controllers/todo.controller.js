sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "sap/ui/model/json/JSONModel",
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/m/Dialog',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text'
], function (Controller, JSONModel, MessageToast, Fragment, Dialog, Button, ButtonType, Text) {
    "use strict";

    return Controller.extend("SapUI5Tutorial.Application.Main.views.todo", {
        onInit: function () {
            databaseHandler.createUserDB();
        },

        onAddTicket: function () {
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("addTicket");
            location.reload();
        },

        onAddProject: function(){
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("project");
            location.reload();
        }
    });
});