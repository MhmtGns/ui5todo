var databaseHandler = {
    db: null,

    createUserDB: function () {
        this.db = window.openDatabase(
            "UserDB",
            "1.0",
            "User Database",
            1000000);
        this.db.transaction(
            function (tx) {             
                tx.executeSql('CREATE TABLE IF NOT EXISTS USERS (name TEXT, username TEXT , email TEXT PRIMARY KEY, password TEXT, position TEXT, image TEXT)',
                    [],
                    function (tx, results) { },
                    function (tx, error) {
                        console.log("Error while creating the table: " + error.message);
                    }
                );
                tx.executeSql('INSERT INTO USERS (name, username, email, password, position) VALUES ("mehmet","mehmet", "mehmet.gunes@detaysoft.com", "123456", "Leader")',
                    [],
                    function (tx, results2) { },
                    function (tx, error) { }
                );                
            },
            function (error) {
                console.log("Transaction error: " + error.message);
            },
            function () {
                console.log("Create DB transaction completed successfully");
            }
        );
    },

    createToDoDB: function () {
        this.db = window.openDatabase(
            "UserDB",
            "1.0",
            "User Database",
            1000000);
        this.db.transaction(
            function (tx) {             
                tx.executeSql('CREATE TABLE IF NOT EXISTS TODO (ticketMetni TEXT, ticketDurumu TEXT, sorumluKisi TEXT, oncelik TEXT, olusturanKullanıcı TEXT, olusturulanTarih DATE, sonDegisiklik DATE)',
                    [],
                    function (tx, results) { },
                    function (tx, error) {
                        console.log("Error while creating the table: " + error.message);
                    }
                );                             
            },
            function (error) {
                console.log("Transaction error: " + error.message);
            },
            function () {
                console.log("Create DB transaction completed successfully");
            }
        );
    },

    createToDoSDB: function () {
        this.db = window.openDatabase(
            "UserDB",
            "1.0",
            "User Database",
            1000000);
        this.db.transaction(
            function (tx) {             
                tx.executeSql('CREATE TABLE IF NOT EXISTS TODOS (ticketSorumlu TEXT, ticketMetni TEXT, PRIMARY KEY (rowid), FOREIGN KEY (ticketMetni) REFERENCES TODO(ticketMetni))',
                    [],
                    function (tx, results) { },
                    function (tx, error) {
                        console.log("Error while creating the table: " + error.message);
                    }
                );                             
            },
            function (error) {
                console.log("Transaction error: " + error.message);
            },
            function () {
                console.log("Create DB transaction completed successfully");
            }
        );
    },
    
    createProjectDB: function () {
        this.db = window.openDatabase(
            "UserDB",
            "1.0",
            "User Database",
            1000000);
        this.db.transaction(
            function (tx) {             
                tx.executeSql('CREATE TABLE IF NOT EXISTS PROJECTS (projectName TEXT PRIMARY KEY, projectSupervisor TEXT, beganDate TEXT, finishDate TEXT)',
                    [],
                    function (tx, results) { },
                    function (tx, error) {
                        console.log("Error while creating the table: " + error.message);
                    }
                );                             
            },
            function (error) {
                console.log("Transaction error: " + error.message);
            },
            function () {
                console.log("Create DB transaction completed successfully");
            }
        );
    },

}