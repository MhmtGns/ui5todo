var Project = {

    AddProject: function (projectName, projectSupervisor, beganDate, finishDate) {
        return addProjectPromise = new Promise(
            function (resolve, reject) {

                databaseHandler.db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO PROJECTS (projectName,projectSupervisor,beganDate,finishDate) VALUES (?,?,?,?)',
                        [projectName, projectSupervisor, beganDate, finishDate], function (tx, results) {
                            resolve("Successfull add project");
                        }, function (tx, error) {
                            console.log("errrrorrrrr")
                        });
                });
            }
        )
    },    

    TableProject: function () {
        return modalPromise = new Promise(
            function(resolve, reject){
                databaseHandler.db.transaction(function (tx){
                    tx.executeSql('SELECT rowid,projectName,projectSupervisor,beganDate,finishDate FROM PROJECTS', [], function (tx, results){
                        resolve(results);
                    },null)
                })
            }
        )
    },

    RemoveProject: function(rowid){
        return removeProject = new Promise(
            function(resolve, reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('DELETE FROM PROJECTS WHERE rowid = ?',[rowid],function(tx,results){
                        resolve("Successfull delete projects");
                    },null)
                })
            }
        )
    },

    EditProject: function(rowid,projectName,beganDate,finishDate){
        return editProject = new Promise(
            function(resolve, reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('UPDATE PROJECTS SET projectName = ?, beganDate = ?, finishDate = ? WHERE rowid = ?',[projectName,beganDate,finishDate,rowid], function(tx,results){
                        resolve("Successfull update project");
                    })
                })
            }
        )
    }




}