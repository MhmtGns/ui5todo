var User = {
    UsernameLogin: function(username,password){
        return usernameLoginPromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('SELECT * FROM USERS WHERE username = ? AND password = ?', [username,password],function(tx,results){
                        if(results.rows.length > 0){
                            resolve(results)
                        }
                        else{
                            reject("no such user")
                        }
                    })
                })
            }
        )
    },

    EmailLogin: function(email,password){
        return emailLoginPromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('SELECT * FROM USERS WHERE email = ? AND password = ?', [email, password],function(tx,results){
                        if(results.rows.length > 0){
                            resolve(results)
                        }
                        else{
                            reject("no such user")
                        }
                    })
                })
            }
        )
    },

    AddEmployee: function(employeeName,employeeUsername,employeeEmail,employeePassword,employeePosition,employeeImage){
        return addEmployeePromise = new Promise(
            function(resolve,reject){
                if(employeeName && employeeUsername && employeeEmail && employeePassword && employeePosition && employeeImage){
                    databaseHandler.db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO USERS (name,username,email,password,position,image) VALUES (?,?,?,?,?,?)', 
                        [employeeName, employeeUsername, employeeEmail, employeePassword, employeePosition, employeeImage], function (tx, results) {
                            resolve("Successfull Add Employee");
                        }, function(tx,error){
                            console.log("errrrorrrrr")
                        });
                    });
                }
                else{
                    reject("Please fill all input");
                }
            }
        )
    },

    ListEmployee: function(){
        return listEmployeePromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('SELECT rowid,name,username,email,position,image FROM USERS WHERE position = ? ',["Consultant"],function(tx,results){
                        resolve(results);
                    })
                })
            }
        )
    },

    EditEmployee: function(rowid,employeeName,employeeUsername,employeeEmail,employeeImage){
        return editEmployeePromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('UPDATE USERS SET name = ?, username = ?, email = ?, image = ? WHERE rowid = ?',[employeeName,employeeUsername,employeeEmail,employeeImage,rowid],function(tx,results){
                        resolve("succesfull update");
                    })
                })
            }
        )
    },

    DeleteEmployee: function(rowid){
        return editEmployeePromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('DELETE FROM USERS WHERE rowid = ?',[rowid],function(tx,results){
                        resolve("succesfull delete");
                    })
                })
            }
        )
    }



}