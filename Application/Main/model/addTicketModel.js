var AddTicket = {

    selectKimden: function(){
        return selectKimdenPromise = new Promise(
            function(resolve, reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('SELECT rowid, username FROM USERS', [], function(tx,results){
                        resolve(results);
                    })
                })
            }
        )
    },

    MultiInput: function (_this, vboxid, multiId) {
        var vbox = _this.getView().byId(vboxid);
        vbox.addItem(new sap.m.MultiInput(multiId, {            
            suggestionItems: {
                path: '/userList',
                template: new sap.ui.core.Item({ key: '{rowid}', text: '{username}' })
            },
        }))
    },

    AddTicket: function(ticketMetni,ticketDurumu,oncelik,olusturanKullanici,olusturulanTarih,sonDegisiklik){
        return addTicketPromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('INSERT INTO TODO (ticketMetni,ticketDurumu,oncelik,olusturanKullanici,olusturulanTarih,sonDegisiklik) VALUES (?,?,?,?,?,?)',
                    [ticketMetni,ticketDurumu,oncelik,olusturanKullanici,olusturulanTarih,sonDegisiklik],function(tx,results){
                        resolve("basarılı");
                    })
                })
            }
        )
    },

    AddTicketSorumlu: function(ticketMetni,ticketSorumlu){
        return addTicketSPromise = new Promise(
            function(resolve,reject){
                databaseHandler.db.transaction(function(tx){
                    tx.executeSql('INSERT INTO TODOS (ticketMetni, ticketSorumlu) VALUES (?,?)',[ticketMetni,ticketSorumlu],function(tx,results){
                        resolve("Sorumlu ekleme basarılı");
                    })
                })
            }
        )
    }
    

}