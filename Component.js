'use strict'
jQuery.sap.require('SapUI5Tutorial.Router')
sap.ui.define(
  [
    'sap/ui/core/UIComponent',
    'sap/ui/model/json/JSONModel'
  ],
  function (UIComponent, JSONModel) {
    return UIComponent.extend('SapUI5Tutorial.Component', {
      metadata: {
        routing: {
          config: {
            routerClass: SapUI5Tutorial.Router,
            viewType: 'XML',
            targetAggregation: 'pages',
            clearTarget: false
          },
          routes: [
          {
            pattern: '',
            viewPath: 'SapUI5Tutorial.Application.Main.views',
            name: 'loginPage',
            view: 'loginPage',
            targetControl: 'masterAppView'
          },
          {
            pattern: 'ToDo',
            viewPath: 'SapUI5Tutorial.Application.Main.views',
            name: 'ToDo',
            view: 'container',
            targetControl: 'masterAppView',
            subroutes:[{
              pattern: 'ToDo/Employee',
              viewPath: 'SapUI5Tutorial.Application.Main.views',
              name: 'ToDo/Employee',
              view: 'employee',
              targetControl: 'newMasterAppView'
            },
            {
              pattern: 'ToDo/Todo',
              viewPath: 'SapUI5Tutorial.Application.Main.views',
              name: 'ToDo/Todo',
              view: 'todo',
              targetControl: 'newMasterAppView'
            }]
          },
          {
            pattern: 'addTicket',
            viewPath: 'SapUI5Tutorial.Application.Main.views',
            name: 'addTicket',
            view: 'addTicket',
            targetControl: 'masterAppView'
          },
          {
            pattern: 'project',
            viewPath: 'SapUI5Tutorial.Application.Main.views',
            name: 'project',
            view: 'project',
            targetControl: 'masterAppView'
          },
          
        ]
        }
      },
      init: function () {
        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);
        var mConfig = this.getMetadata().getConfig();
        this.getRouter().initialize();
      },
      createContent: function () {
        var oViewData = {
          component: this
        }
        return sap.ui.view({
          viewName: 'SapUI5Tutorial.RootApp',
          type: sap.ui.core.mvc.ViewType.XML,
          id: 'app',
          viewData: oViewData
        })
      }
    })
  }
)